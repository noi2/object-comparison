import sys
from typing import Union, Any


class Comparer:
    """Класс для сравнения объектов и вывода разницы"""
    NUMBERS_TYPES = (int, float, complex)
    SUPPORT_TYPES = (list, tuple, dict, str, bool, *NUMBERS_TYPES)
    NAME_OF_ROOT_LOCATION = 'start_object'

    def __init__(self):
        self._identical = True

    @property
    def identical(self) -> bool:
        """Getter для вывода результата сравнения объектов"""
        return self._identical

    @identical.setter
    def identical(self, value: bool) -> None:
        """Setter для результата сравнения объектов

        Args:
            value: Булеан значение

        Notes:
            Если в результате рекурсивного сравнения однажды поменялось значение на False, больше не меняем его,
            т.к. объекты уже не идентичны

        """
        if self._identical is True:
            self._identical = value

    def _check_obj_type_in_supports(self, obj: Any, location: str = NAME_OF_ROOT_LOCATION) -> type:
        """Проверка типа объекта на поддерживаемые типы данных для сравнения

        Args:
            obj: Объект сравнения
            location: Локация (позиция) внутри объекта, необходима для вывода разницы объектов в консоль

        Notes:
            Если тип объекта не поддерживается, выводим это и останавливаем исполнение, т.к. непонятно как дальше
            работать с объектом

        Returns:
            Тип данных объекта

        """
        if (obj_type := type(obj)) not in self.SUPPORT_TYPES:
            self._not_identical(
                location,
                result=f"{obj.__class__.__name__} не является поддерживаемым типом данных для сравнения объектов"
            )
            sys.exit(1)
        return obj_type

    def recursive_comparer(self, obj1: Union[SUPPORT_TYPES], obj2: Union[SUPPORT_TYPES],
                           location: str = NAME_OF_ROOT_LOCATION) -> bool:
        """Рекурсивное сравнение объектов многих встроенных типов данных и вывод результата сравнения

        Args:
            obj1: Первый объект сравнения
            obj2: Второй объект сравнения
            location: Локация (позиция) внутри объекта, необходима для вывода разницы объектов в консоль

        Returns:
            Результат сравнения объектов в булеан

        """
        object_1_type, object_2_type = self._check_obj_type_in_supports(obj1,
                                                                        location), self._check_obj_type_in_supports(
            obj2, location)

        if object_1_type != object_2_type:
            self._not_identical(
                location,
                result=f"Сравниваются различные типы объектов {object_1_type.__name__} и {object_2_type.__name__}"
            )

        elif object_1_type == dict:
            if (obj1_keys := obj1.keys()) != (obj2_keys := obj2.keys()):
                s1, s2 = set(obj1_keys), set(obj2_keys)
                self._not_identical(
                    location,
                    result=f"На месте словаря под {'ключом(ами) ' + str(s1 - s2) if s1 - s2 else 'пустым ключом'} первого объекта, "
                           f"присутствует(ют) {'ключ(и) ' + str(s2 - s1) if s2 - s1 else 'пустой ключ'} второго объекта"
                )
                conjunction_keys = s1 & s2
            else:
                conjunction_keys = frozenset(obj1_keys)

            for k in conjunction_keys:
                self.recursive_comparer(obj1[k], obj2[k], location=f"{location}['{k}']")

        elif object_1_type in (list, tuple):
            if (obj1_len := len(obj1)) != (obj2_len := len(obj2)):
                self._not_identical(
                    location,
                    result=f"Длина последовательности первого объекта ({obj1_len}) "
                           f"не совпадает с длинной последовательности второго объекта ({obj2_len})"
                )

            min_obj_len = min(obj1_len, obj2_len)

            for i in range(min_obj_len):
                self.recursive_comparer(obj1[i], obj2[i], location=f'{location}[{i}]')

        else:
            if obj1 != obj2:
                self._not_identical(location,
                                    result=f"{obj1} не равен {obj2}, тип данных {object_1_type.__name__}")

        return self.identical

    def _not_identical(self, location: str, result: str) -> None:
        """Обозначаем, что объекты не идентичны и выводим результат в консоль

        Args:
            location: Локация (позиция) внутри объекта, необходима для вывода разницы объектов в консоль
            result: Причина несоответствия

        """
        self.identical = False
        self._print_bad_result(location, result)

    def _print_bad_result(self, location: str, result: str) -> None:
        """Вывод результата в консоль

        Args:
            location: Локация (позиция) внутри объекта, необходима для вывода разницы объектов в консоль
            result: Причина несоответствия

        """
        print(f"Дислокация: {location + ' ' * 3}"
              f"Несовпадение: {result}")


if __name__ == '__main__':
    film1 = {
        "title": "Освобожденный Джанго",
        "budget": 1000000,
        "personal": {"Adams": "актер", "Bred": "монтажер", "Tomas": "сценарист"},
        "18+": True,
        "ratings": [8.2, 7.9, 8.5],

    }
    film2 = {
        "ratings": [4.2, 7.9, 8.2],
        "title": "Джанго освобождённый",
        "dudget": 53455676,
        "personal": {"Tomas": "сценарист-режиссер", "Adams": "актер", "Bred": "монтажер", "Dooglas": "оператор"},
        "18+": False
    }

    if Comparer().recursive_comparer(film1, film2):
        print("Объекты совпадают")
